You must have a patched supybot, see:

https://sourceforge.net/tracker/?func=detail&aid=2960892&group_id=58965&atid=489447

Run:

$ mkdir logs build supybot
$ cd supybot
$ supybot-wizard
# copy in the required supybot bits from supybot-required.conf
# create a file build/builder.conf, see build.conf.sample
$ ./supervisor-session supervisor.conf

