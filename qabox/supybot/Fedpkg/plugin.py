###
# Licensed under the new-BSD license (http://www.opensource.org/licenses/bsd-license.php)
# Copyright (C) 2010 Red Hat, Inc.
# Written by Colin Walters <walters@verbum.org>
#
###

import supybot.conf as conf
import supybot.utils as utils
from supybot.commands import *
import supybot.plugins as plugins
import supybot.ircutils as ircutils
import supybot.callbacks as callbacks

import dbus
from dbus.mainloop.glib import DBusGMainLoop
DBusGMainLoop(set_as_default=True)

AUTOBUILD_SERVICE = 'org.fedoraproject.FedpkgAutoBuilder'
AUTOBUILD_OBJPATH = '/org/fedoraproject/FedpkgAutoBuilder'

def undbus_dict_strings(dictvalue):
    r = {}
    for k in dictvalue:
        r[unicode(k)] = unicode(dictvalue[k])
    return r

class Fedpkg(callbacks.Plugin):
    """Add the help for "@plugin help Fedpkg" here
    This should describe *how* to use this plugin."""
    def __init__(self, irc):
        callbacks.Plugin.__init__(self, irc)
        self.__irc = irc
        self._builddir = conf.supybot.plugins.Fedpkg.builddir()
        
        bus = dbus.SessionBus()
        self._bus_proxy = dbus.Interface(bus.get_object('org.freedesktop.DBus', '/org/freedesktop/DBus'),
                                         'org.freedesktop.DBus')
        self._bus_proxy.connect_to_signal('NameOwnerChanged', self.__on_name_owner_changed)
        try:
            self.__reload_proxy()
        except:
            self._autobuild_proxy = None

    def __on_name_owner_changed(self, name, prev_owner, new_owner):
        if name != AUTOBUILD_SERVICE:
            return
        if new_owner == '':
            del self._autobuild_proxy
            self._autobuild_proxy = None
            return
        self.__reload_proxy()
        
    def __reload_proxy(self):
        bus = dbus.SessionBus()
        proxy = bus.get_object(AUTOBUILD_SERVICE, AUTOBUILD_OBJPATH)
        self._autobuild_proxy = dbus.Interface(proxy, AUTOBUILD_SERVICE)
        self._modules = self._autobuild_proxy.GetModules()
        self._autobuild_proxy.connect_to_signal('StateChanged', self.__on_builder_state_changed)

    def status(self, irc, msg, args):
        if self._autobuild_proxy is None:
            irc.reply(AUTOBUILD_SERVICE + ' is not running')
            return
        (status, statusdata) = self._autobuild_proxy.GetStatus()
        (state, statedata) = self._autobuild_proxy.GetState()
        irc.reply("[%s %r] build of %s is currently %s (%r)" % (status, undbus_dict_strings(statusdata), 
                                                                self._modules[-1], state,
                                                                undbus_dict_strings(statedata)))
        
    def build(self, irc, msg, args):
        if self._autobuild_proxy is None:
            irc.reply(AUTOBUILD_SERVICE + ' is not running')
            return
        self._autobuild_proxy.Build(True, reply_handler=lambda *args: irc.reply(args[0]), error_handler=lambda *args: True)
        
    def __on_builder_state_changed(self, state, statedata):
        for channel in self.__irc.state.channels:
            self.__irc.reply("build of %s has changed state to %s (%r)", self._modules[1], state,
                             undbus_dict_strings(statedata))


Class = Fedpkg


# vim:set shiftwidth=4 softtabstop=4 expandtab textwidth=79:
