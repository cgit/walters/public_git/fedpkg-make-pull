###
# Copyright (c) 2010, Colin Walters
# All rights reserved.
#
#
###

from supybot.test import *

class FedpkgTestCase(PluginTestCase):
    plugins = ('Fedpkg',)


# vim:set shiftwidth=4 tabstop=4 expandtab textwidth=79:
